<?php


namespace App;


class East extends Direction
{
    public function rotateLeft(): Direction
    {
        return self::north();
    }

    public function rotateRight(): Direction
    {
        return self::south();
    }

    public function move(Coordinates $coordinate, $displacement): Coordinates
    {
        return $coordinate->moveAlongX( $displacement);
    }
}