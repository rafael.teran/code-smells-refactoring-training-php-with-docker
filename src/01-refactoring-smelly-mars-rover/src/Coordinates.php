<?php


namespace App;


class Coordinates
{
    private int $x;
    private int $y;

    public function __construct(int $x, int $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    public function moveAlongY(int $displacement): Coordinates
    {
        return new Coordinates($this->x, $this->y + $displacement);
    }

    public function moveAlongX(int $displacement): Coordinates
    {
        return new Coordinates($this->x + $displacement, $this->y);
    }


}