<?php


namespace App;


class West extends Direction
{
    public function rotateLeft(): Direction
    {
        return self::south();
    }

    public function rotateRight(): Direction
    {
        return self::north();
    }

    public function move(Coordinates $coordinate, $displacement): Coordinates
    {
        return $coordinate->moveAlongX( - $displacement);
    }
}