<?php

namespace App;

use App\North;
use App\South;
use App\West;
use App\East;
use PHPUnit\Util\Exception;

abstract class Direction
{
    public static function create(string $direction): Direction
    {
        switch ($direction) {
            case "N":
                return self::north();
            case "S":
                return self::south();
            case "W":
                return self::west();
            default:
                return self::east();
        }
    }

    protected static function north(): North
    {
        return new North();
    }

    protected static function south(): South
    {
        return new South();
    }

    protected static function west(): West
    {
        return new West();
    }

    protected static function east(): East
    {
        return new East();
    }

    abstract public function rotateLeft(): Direction;

    abstract public function rotateRight(): Direction;

    abstract public function move(Coordinates $coordinate, $displacement): Coordinates;

}