<?php

declare(strict_types=1);

namespace App;
use App\Direction;

class Rover
{
    const DISPLACEMENT = 1;
    const COMMAND_LEFT = 'l';
    const COMMAND_RIGHT = 'r';
    const COMMAND_FORWARD = 'f';
    private Direction $direction;
    private Coordinates $coordinates;

    public function __construct(int $x, int $y, string $direction)
    {
        $this->coordinates = new Coordinates($x, $y);
        $this->direction = Direction::create($direction);
    }

    public function receive(string $commandsSequence): void
    {
        $commandsSequenceLenght = strlen($commandsSequence);

        for ($i = 0; $i < $commandsSequenceLenght; ++$i) {
            $command = substr($commandsSequence, $i, 1);

            if( $command === self::COMMAND_LEFT ) {
                // Rotate Rover
                $this->direction = $this->direction->rotateLeft();
            } else if( $command === self::COMMAND_RIGHT ) {
                // Rotate Rover
                $this->direction = $this->direction->rotateRight();
            } else if( $command === self::COMMAND_FORWARD ) {
                $this->coordinates = $this->direction->move($this->coordinates, self::DISPLACEMENT);
            } else {
                $this->coordinates = $this->direction->move($this->coordinates, -self::DISPLACEMENT);
            }
        }

    }

}
